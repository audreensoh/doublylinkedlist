#include "DoublyLinkedList.h"
#include "Player.h"
#include"Database.h"
#include"Menu.h"
#include<iostream>

#define _CRTDBG_MAP_ALLOC

#include<stdlib.h>
#include<crtdbg.h>

using std::cin;
using std::endl;
using std::cout;

int main()
{
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif


	DLinkedList<Player>list;
	Database myDatabase(list);


	bool main = false;
	do{
		main=showMainMenu(myDatabase);
	} while (main = false);
		
		
	cin.ignore();
	return 0;

}