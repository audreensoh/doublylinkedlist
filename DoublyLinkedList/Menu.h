#ifndef	MENU_H
#define MENU_H

/**
Contains functions for showing main menu and parsing the user choice
**/

#include<string>
#include"Database.h"

bool showMainMenu(Database myDatabase);
void menu();
Player addPlayer();
int userChoiceInt();
std::string userChoiceString();
#endif