#ifndef DLINKEDLIST_H
#define DLINKEDLIST_H

/**
custom doubly linked list template class includes:
-DListNode to model a node in the list
-DLinkedList to model the list
-DListIterator to iterate through the list
**/

template<class Datatype> class DListNode;
template<class Datatype> class DLinkedList;
template<class Datatype> class DListIterator;

//Basic doulbly linked list node class
template<class Datatype>
class DListNode
{
private:
	//The data stored in each node
	Datatype m_data;
	//A pointer to the next node in the list
	DListNode<Datatype>* m_next;
	//A pointer to the previous node in the list
	DListNode<Datatype>* m_previous;
public:
	//constructor with empty nodes
	DListNode() :m_next(0), m_previous(0)
	{
	}

	//getters and setters to access the private variables
	//set data of current node
	void setData(Datatype p_data)
	{
		m_data = p_data;
	}

	//get data of current node
	Datatype& getData()
	{
		return m_data;
	}

	//set the pointer to point to the next node
	void setNext(DListNode<Datatype>* p_node)
	{
		m_next = p_node;
	}

	//get the next node the pointer is pointing to
	DListNode<Datatype>* getNext()
	{
		return m_next;
	}

	//set the pointer to point to the previous node
	void setPrevious(DListNode<Datatype>* p_node)
	{
		m_previous = p_node;
	}

	//get the previous node the pointer is pointing to
	DListNode<Datatype>* getPrevious()
	{
		return m_previous;
	}

	//set current node (the head node of the list)'s pointer to the previous to null pointer
	void setStartPreviousToNull()
	{
		m_previous = nullptr;
	}

	//set current node (the tail node of the list)'s pointer to the next to null pointer
	void setEndNextToNull()
	{
		m_next = nullptr;
	}

	//print out to check the addresses if its added or removed correctly
	void print()
	{
		cout << "Previous pointer: " << getPrevious() << endl;
		cout << "Current pointer: " << &m_data<< endl;
		cout << "Next pointer: " << getNext() << endl;
	}

};

//Doubly linked list container class
template<class Datatype>
class DLinkedList
{
private:
	//Pointer to the head node
	DListNode<Datatype>* m_head;
	//Pointer to the tail node
	DListNode<Datatype>* m_tail;
	//A counter to count the total nodes in the list
	int m_count = 0;
public:
	//Constuructor to create an empty list
	DLinkedList() :m_head(0), m_tail(0), m_count(0)
	{
	}

	//deconstructor to destroy list to free memory
	~DLinkedList()
	{
		//temporary node pointer
		DListNode<Datatype>* itr = m_head;
		DListNode<Datatype>* next;

		while (itr != nullptr)
		{
			//save pointer to the next node
			next = itr->getNext();
			//delete the current node
			cout << "Pointer: " << itr << endl;
			delete itr;

			//make the next node the current node
			itr = next;
		}
		//std::cin.ignore();
	}

	//To add a node to the end of the linked list
	//finding node O(n), inserting node O(c)
	void append(Datatype p_data)
	{
		//if m_head is empty
		if (!m_head)
		{
			//create a new headnode
			insertFirstElement(p_data);
		}
		else
		{
			//create a new node
			DListNode<Datatype>* newNode = new DListNode<Datatype>;
			//set newNode's previous node to point to the tail node 
			newNode->setPrevious(m_tail);
			//set newNode's next node to null pointer
			newNode->setEndNextToNull();
			//set newNode data 
			newNode->setData(p_data);
			//make newNode the tail node
			m_tail = newNode;
			//set the previous node's next node pointer point at the tail node
			m_tail->getPrevious()->setNext(m_tail);
			cout << endl;
			cout << "Element Inserted(Tail)" << endl;
			m_tail->print();
		}
		m_count++;
	}
	
	//to add Node to the head of the list
	//O(c)
	void insertFirstElement(Datatype p_data)
	{
		DListNode<Datatype>* newNode = new DListNode<Datatype>;
		newNode->setData(p_data);
		newNode->setStartPreviousToNull();
		newNode->setEndNextToNull();
		m_head = newNode;
		m_tail = newNode;
		//cout << endl;
		cout << "Element Inserted(Head)" << endl;
		newNode->print();
	}

	//O(c)
	void prepend(Datatype p_data)
	{
		DListNode<Datatype>* newNode = new DListNode<Datatype>;
		newNode->setData(p_data);
		m_head->setPrevious(newNode);
		newNode->setNext(m_head);
		newNode->setStartPreviousToNull();
		m_head = newNode;
		m_count++;
		cout << "Element Inserted(Head)" << endl;
		cout << endl;
		newNode->print();
	}

	//Function to insert a node in particular position
	//finding node O(n), inserting node O(c)
	void Insert(Datatype p_data, int p_position)
	{
		//reference http://www.sanfoundry.com/cpp-program-implement-doubly-linked-list/
		Timer myTimer;
		myTimer.Reset();
		//check if m_head is empty, insert a node as a head node and also the tail node
		if (m_head==0)
		{
			insertFirstElement(p_data);
			m_count++;
		}
		else if (p_position == 0 || p_position == 1)
		{
			//If p_position is 0, then insert the node as the first node
			prepend(p_data);
		}
		else if (p_position > m_count)
		{
			//if p_position is larger than the list size then append the node at the end of the list
			append(p_data);
		}
		else if (p_position == m_count)
		{
			DListNode<Datatype>* newNode = new DListNode<Datatype>;
			newNode->setData(p_data);
			newNode->setPrevious(m_tail->getPrevious());
			m_tail->getPrevious()->setNext(newNode);
			newNode->setNext(m_tail);
			m_tail->setPrevious(newNode);
			
			cout << "Element Inserted" << endl;
			newNode->print();
			
			m_count++;

		}
		else
		{
			//create a new node
			DListNode<Datatype>* newNode = new DListNode<Datatype>;
			//create a temperory node for iterator 
			DListNode<Datatype>* iter;
			//set temporary node to head node
			iter = m_head;
			//loop till the position you wish to add
			for (int i = 0; i < p_position - 2; i++)
			{
				iter = iter->getNext();
			}
			//now the iter node is at the place where the node is to be insert
			//set the newNode's next node pointer to point to the iter's next node
			newNode->setNext(iter->getNext());
			//set the next node's previous pointer to newNode
			newNode->getNext()->setPrevious(newNode);
			iter->setNext(newNode);
			newNode->setPrevious(iter);
			cout << "Element Inserted" << endl;
			newNode->setData(p_data);
			m_count++;
			newNode->print();
		}
		std::cout << "time taken(Ms): " << myTimer.GetMS() << std::endl;
	}

	

	//Function that remove a node from particular position
	//finding node O(n), deleting node O(c)
	void remove(int p_position)
	{
		Timer myTimer;
		myTimer.Reset();

		//check if list is empty
		if (m_count == 0)
		{
			cout << "List is empty." << endl;
		}
		else
		{
			//if there is only one node in the list then delete that node and set the head and tail pointer as null pointer
			if (m_count == 1)
			{
				emptyList();
			}
			else if (p_position == 0)
			{
				//if p_position is 0 then remove the head node
				removeHead();

			}
			else if (p_position >= m_count)
			{
				//if p_position is larger or equals to the size of the list
				//then remove the tail node
				removeTail();
			}
			else
			{
				//else
				//iterate through the list to the position and delete node
				DListNode<Datatype>* iter;

				iter = m_head;
				for (int i = 0; i < p_position - 1; i++)
				{
					iter = iter->getNext();
				}
				iter->print();
				iter->getPrevious()->setNext(iter->getNext());
				iter->getNext()->setPrevious(iter->getPrevious());
				delete iter;
				m_count--;
				cout << "Element Deleted" << endl;
			}
		}
		std::cout << "time taken(Ms): " << myTimer.GetMS() << std::endl;
	}

	//Removing node with iterator
	//finding node O(n), deleting node O(c)
	void removeItr(DListIterator<Datatype> p_iter)
	{
		Timer myTimer;
		myTimer.Reset();

		if (m_count == 0)
		{
			cout << "List is empty." << endl;
		}
		else
		{
			//if there is only one node in the list then delete that node and set the head and tail pointer as null pointer
			if (m_count == 1)
			{
				emptyList();
			}
			else if (p_iter.getNode() == m_head)
			{
				//if p_position is 0 then remove the head node
				removeHead();

			}
			else if (p_iter.getNode() ==m_tail)
			{
				//if p_position is larger or equals to the size of the list
				//then remove the tail node
				removeTail();
			}
			else
			{
				//else
				//iterate through the list to the position and delete node
				DListNode<Datatype>* iter;
				iter = p_iter.getNode();
				//iter->print();
				iter->getPrevious()->setNext(iter->getNext());
				iter->getNext()->setPrevious(iter->getPrevious());
				delete iter;
				m_count--;
				cout << "Element Deleted" << endl;
			}
		}
		std::cout << "time taken(Ms): " << myTimer.GetMS() << std::endl;
		
	}
	//deleting node O(c)
	void emptyList()
	{
		DListNode<Datatype>* tempNode;
		tempNode = m_head = m_tail;
		delete tempNode;
		m_head = nullptr;
		m_tail = nullptr;
		m_count=0;
		cout << "Element Deleted" << endl;
	}

	//deleting node O(c)
	void removeHead()
	{
		DListNode<Datatype>* tempNode;
		tempNode = m_head;
	
		m_head = m_head->getNext();
		delete tempNode;
		m_head->setStartPreviousToNull();
		m_count--;
		cout << "Element Deleted" << endl;
	}

	//deleting node O(c)
	void removeTail()
	{
		DListNode<Datatype>* tempNode;
	
		//cout << "Remove tail" << endl;
		tempNode = m_tail;
		m_tail = m_tail->getPrevious();
		delete tempNode;
		m_tail->setEndNextToNull();
		m_count--;
		//cout << m_count << endl;
		cout << "Element Deleted" << endl;
	}

	//Gets an iterator pointing to the start of the current list
	//O(c), as the head node is always the first node
	DListIterator<Datatype> GetIterator()
	{
		return DListIterator<Datatype>(this, m_head);
	}

	//To get the total number of nodes in the list
	int count()
	{
		return m_count;
	}

	//Function to get the head node in the list
	DListNode<Datatype>* getHead()
	{
		return m_head;
	}

	void printList()
	{
		DListNode<Datatype>* iter;
		cout << "Print whole list" << endl;
		iter = m_head;
		while (iter != nullptr)
		{
			
			iter->print();

			if (iter == nullptr)
			{
				cout << "This is null printer" << endl;
				std::cin.ignore();
			}
			iter = iter->getNext();
		}
			

	}
};


template <class Datatype>
class DListIterator
{
private:
	DLinkedList<Datatype>* m_list;
	DListNode<Datatype>* m_node;
public:
	DListIterator(DLinkedList<Datatype>* p_list = 0, DListNode<Datatype>* p_node = 0)
	{
		m_list = p_list;
		m_node = p_node;
	}

	DLinkedList<Datatype>* getList()
	{
		return m_list;
	}

	DListNode<Datatype>* getNode()
	{
		return m_node;
	}


	//moves the iterator to start of the list
	void start()
	{
		if (m_list != 0)
		{
			m_node = m_list->getHead();

		}

	}

	//moves the iterator forward
	void forth()
	{
		if (m_node != 0)
		{
			m_node = m_node->getNext();
		}
	}

	//If the iterator is valid
	bool valid()
	{
		return(m_node != nullptr);
	}


	//return item that iterator points to
	Datatype& Item()
	{
		//cout << "Enter Item" << endl;
		return m_node->getData();
		
	}

	void moveToPrevious()
	{
		if (m_list->count() == 1)
		{
			m_node = m_list->getHead();
		}
		else if (m_list != 0)
		{
			m_node = m_node->getPrevious();
		}


	}

	void moveToNext()
	{
		if (m_list != 0)
		{
			m_node = m_node->getNext();
		}

	}

	//return next item that iterator points to
	Datatype& NextItem()
	{
		//cout << "Enter Item" << endl;
		return m_node->getNext()->getData();

	}

};

#endif