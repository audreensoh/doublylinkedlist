#include "Database.h"
#include "Timer.h"
#include<iostream>
#include<string>
#include<regex>
#include<vector>

using std::cout;
using std::endl;
using std::vector;
using std::string;

//constructor
Database::Database()
{
		
}

Database::Database(DLinkedList<Player> p_list)
{
	m_list = p_list;
}

//Destructor
Database::~Database()
{
}

//insert player
void Database::insert(Player p_player, int p_position)
{
	m_list.Insert(p_player, p_position);

}

//remove player
void Database::remove(int p_position)
{
	m_list.remove(p_position);
}

void Database::removeItr(DListIterator<Player> p_iter)
{

	m_list.removeItr(p_iter);

}

//search single and return vector of players at particular level
//O(n), needs to go through the whole list to get every player at particular level
vector<Player> Database::searchSingle(int p_level)
{
	Timer myTimer;
	myTimer.Reset();
	vector<Player> sameLevel;
	DListIterator<Player> iter = m_list.GetIterator();
	for (iter.start(); iter.valid(); iter.forth())
	{
		if (iter.Item().getlevel() == p_level)
		{
			sameLevel.push_back(iter.Item());

			/*cout << "FirstName: " << iter.Item().getFirstName() << endl;
			cout << "LastName : " << iter.Item().getLastName() << endl;
			cout << "Level    : " << iter.Item().getlevel() << endl;
			cout << "XP       : " << iter.Item().getXp() << endl;
			cout << endl;*/
		}
	}
	//cout << "Exit search single" << endl;
	return sameLevel;
	cout << "time taken(Ms): " << myTimer.GetMS() << endl;
}



//search and print the list of players in particular level or range of level 
//O(n),needs to go through the whole list to get every player at particular level or range of level
void Database::searchRange(std::string p_toSearch)
{
	Timer myTimer;
	myTimer.Reset();
	//using regex and match group to check the input if theres a '-' in the string
	std::tr1::cmatch res;
	std::tr1::regex rx("(\\d){1}(-)*(\\d)*");
	std::tr1::regex_search(p_toSearch.c_str(), res, rx);

	std::string firstNum = res[1];
	std::string secondNum = res[3];

	//change the datatype from string to integer
	int one = atoi(firstNum.c_str());
	int two = atoi(secondNum.c_str());


	int counter = 0;

	//check if group(2) of regex contains a '-'
	//if no it means that its a single search, if yes means its a range search
	if (res[2]!="-")
	{
		myTimer.Reset();
		vector<Player> sameLevel=searchSingle(one);
		if (sameLevel.size() == 0)
		{
			cout << "No player found" << endl;
			return;
		}
		else
		{
			for (int i = 0; i !=sameLevel.size(); i++)
			{
				cout << "FirstName: " << sameLevel[i].getFirstName() << endl;
				cout << "LastName : " << sameLevel[i].getLastName() << endl;
				cout << "Level    : " << sameLevel[i].getlevel() << endl;
				cout << "XP       : " << sameLevel[i].getXp() << endl;
				cout << endl;
			}
		}
	}
	else{

		if (two < one)
		{
			cout << "Second number must be bigger than the first number. e.g. 1-3" << endl;
			return;
		}
		int p_levela = one;
		int p_levelb = two;
		DListIterator<Player> iter = m_list.GetIterator();
		for (iter.start(); iter.valid(); iter.forth())
		{
			myTimer.Reset();
			if (iter.Item().getlevel() >= p_levela && iter.Item().getlevel() <= p_levelb)
			{
				cout << "FirstName: " << iter.Item().getFirstName() << endl;
				cout << "LastName : " << iter.Item().getLastName() << endl;
				cout << "Level    : " << iter.Item().getlevel() << endl;
				cout << "XP       : " << iter.Item().getXp() << endl;
				cout << endl;
				counter++;
			}
		}
	}

	if (counter == 0)
	{
		cout << "No player found"<<endl;
	}

	cout << "time taken(Ms): " << myTimer.GetMS() << endl;
}

//O(n), as it needs to go through the whole iterator to find if there is any same first name
void Database::searchFirstName(std::string p_toSearch)
{
	Timer myTimer;
	myTimer.Reset();
	vector<Player> sameFirstName;

	DListIterator<Player> iter = m_list.GetIterator();
	int counter = 0;
	for (iter.start(); iter.valid(); iter.forth())
	{
		if (iter.Item().getFirstName()==p_toSearch)
		{
			sameFirstName.push_back(iter.Item());
			cout << "Added item to vector" << iter.Item().getFirstName() << endl;
			counter++;
		}
	}
	if (counter == 0)
	{
		cout << "No player found with this name.Please try again." << endl;
	}
	else
	{
		cout << counter << endl;
		for (int i = 0; i < counter; i++)
		{
			cout << "FirstName: " << sameFirstName[i].getFirstName() << endl;
			cout << "LastName : " << sameFirstName[i].getLastName() << endl;
			cout << "Level    : " << sameFirstName[i].getlevel() << endl;
			cout << "XP       : " << sameFirstName[i].getXp() << endl;
			cout << endl;
		}

		
	}
	cout << "time taken(Ms): " << myTimer.GetMS() << endl;
}

//O(n), it needs to go through all nodes to search for the same level and total their xp.
void Database::averageXp(int p_level)
{
	Timer myTimer;
	myTimer.Reset();
	vector<Player> sameLevel = searchSingle(p_level);
	int total = 0;
	double average = 0;
	int counter=0;
	
	for (int i = 0; i !=sameLevel.size(); i++)
	{
		total += sameLevel[i].getXp();
		counter++;
	}
	

	if (counter == 0)
	{
		cout << "No player at level " << p_level << endl;
		return;
	}
	average = total / counter;

	cout << "Average XP of level " << p_level << " is " << average << endl;
	cout << "time taken(Ms): " << myTimer.GetMS() << endl;
}

//Remove all duplicates in the list
//O(n)
void Database::removeDuplicates()
{
	Timer myTimer;
	myTimer.Reset();
	DListIterator<Player> iter = m_list.GetIterator();
	for (iter.start(); iter.valid(); iter.forth())
	{
		bool changed =removeDuplicatescheck(iter.Item().getFirstName(), iter.Item().getLastName());
		
		if (changed == true)
		{
			//cout << "Changed and restart iterator" << endl;
			iter.start();
		}
	}
	cout << "All duplicates are removed" << endl;
	cout << "time taken(Ms): " << myTimer.GetMS() << endl;
	std::cin.ignore(2);
}

bool Database::removeDuplicatescheck(string firstName, string lastName)
{
	int count = 0;
	bool changed = false;
	DListIterator<Player> iter = m_list.GetIterator();
	for (iter.start(); iter.valid(); iter.forth())
	{
		//cout << "Count:" << count << endl;
		if (iter.Item().getFirstName() == firstName && iter.Item().getLastName() == lastName)
		{
			//cout << iter.Item().getFirstName() << " " << iter.Item().getLastName() << endl;
			if (count >= 1)
			{
				//iter.getNode()->print();
				removeItr(iter);
				changed = true;
				count = 0;
				iter.start();
				//iter.moveToPrevious();
				
			}
			else
			{
				count++;
			}
			
		}
	}
	return changed;
}

void Database::printAll()
{
	DListIterator<Player> iter = m_list.GetIterator();
	cout << "List Contains: " << endl;
	for (iter.start(); iter.valid();iter.forth())
	{

		//cout << "Enter Iterator" << endl;
		cout << "FirstName: " << iter.Item().getFirstName() << endl;
		cout << "LastName : " << iter.Item().getLastName() << endl;
		cout << "Level    : " << iter.Item().getlevel() << endl;
		cout << "XP       : " << iter.Item().getXp() << endl;
		cout << endl;
	}

	m_list.count();

}
