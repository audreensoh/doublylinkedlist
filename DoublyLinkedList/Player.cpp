#include "Player.h"
#include<iostream>
#include<string>

using std::string;

Player::Player()
{
	m_firstName = "No Name";
	m_lastName = "No Name";
	m_level = 0;
	m_xp = 0;
}
Player::Player(string firstName, string lastName, int level, int xp)
{
	m_firstName = firstName;
	m_lastName = lastName;
	m_level = level;
	m_xp = xp;

	//std::cout << "Player Added" << std::endl;
}

Player::~Player()
{

}

void Player::setFirstName(string firstName)
{
	m_firstName = firstName;
}

void Player::setLastName(string lastName)
{
	m_lastName = lastName;
}

void Player::setLevel(int level)
{
	m_level = level;
}

void Player::setXp(int xp)
{
	m_xp = xp;
}

string Player::getFirstName()
{
	return m_firstName;
}

string Player::getLastName()
{
	return m_lastName;
}

int Player::getlevel()
{
	return m_level;
}

int Player::getXp()
{
	return m_xp;
}

void Player::print()
{
	std::cout << "FirstName: "<< getFirstName() << std::endl;
	std::cout << "LastName : "<< getLastName() << std::endl;
	std::cout << "Level    : " <<getlevel() << std::endl;
	std::cout << "XP       : " << getXp() << std::endl;
}