#include"Menu.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

//show Main menu choices
bool showMainMenu(Database myDatabase)
{
	bool exit = false;
	int choice=0;
	do{
		system("CLS");
		menu();
		int choice = userChoiceInt();

		if (choice == 1)
		{
			Player p = addPlayer();
			cout << "Enter position:" << endl;
			int position = userChoiceInt();
			myDatabase.insert(p, position);
			cin.ignore(2);
		}
		else if (choice == 2)
		{
			cout << "Enter position to delete:" << endl;
			int position = userChoiceInt();
			myDatabase.remove(position);
			cin.ignore(2);
		}
		else if (choice == 3)
		{
			cout << "Press 1 to search level" << endl;
			cout << "Press 2 to search First Name" << endl;
			int searchChoice = userChoiceInt();
			if (searchChoice == 1)
			{
				cout << "Enter level to search(single or range i.e 0-4)" << endl;
				string toSearch;
				cin >> toSearch;
				myDatabase.searchRange(toSearch);
				cin.ignore(2);
			}
			else if (searchChoice == 2)
			{
				cout << "Enter First Name you wish to search" << endl;
				string name = userChoiceString();
				myDatabase.searchFirstName(name);
				cin.ignore(2);
			}
		}
		else if (choice == 4)
		{
			cout << "Enter level you wish to get average:" << endl;
			int level = userChoiceInt();
			myDatabase.averageXp(level);
			cin.ignore(2);
		}
		else if (choice == 5)
		{
			myDatabase.removeDuplicates();
		}
		else if (choice == 6)
		{
			exit = true;
		}
		else
		{
			cout << "Invalid choice entered. Please try again." << endl;
			cin.ignore();
		}
		
	} while (exit != true);
	return exit;
}


void menu()
{
	cout << "Press 1 to insert a new record at a particular position" << endl;
	cout << "Press 2 to delete a record from a particular position" << endl;
	cout << "Press 3 to search the database  and print results" << endl;
	cout << "Press 4 to find the average experience points of players at a particular level" << endl;
	cout << "Press 5 to find and remove all duplicate entries" << endl;
	cout << "Press 6 to quit" << endl;
}

//Function to add player details
Player addPlayer()
{
	Player p;
	cout << "Enter First Name:" << endl;
	string firstName = userChoiceString();
	p.setFirstName(firstName);
	cout << "Enter Last Name:" << endl;
	string lastName = userChoiceString();
	p.setLastName(lastName);
	cout << "Enter Level:" << endl;
	int level = userChoiceInt();
	p.setLevel(level);
	cout << "Enter Xp:" << endl;
	int xp = userChoiceInt();
	p.setXp(xp);

	return p;
}

//to parse if user input is valid
int userChoiceInt()
{
	int choice;
	bool valid = false;

	while (valid == false)
	{
		cout << ">>";
		cin >> choice;
		if (!cin)
		{
			cin.clear();
			cout << "Only integer input allowed.Please try again." << endl;
			cin.ignore();
			
		}
		else
		{
			valid = true;
		}
		
	}
	return choice;
}

//to parse if user input is valid
string userChoiceString()
{
	string choice;
	cout << ">>";
	cin >> choice;
	return choice;
}