#ifndef DATABASE_H
#define DATABASE_H

/**
Contains functions for storing and managing all players.
**/

#include"Player.h"
#include"DoublyLinkedList.h"
#include<vector>

class Database
{
private:
	DLinkedList<Player> m_list;
public:
	Database();
	Database(DLinkedList<Player> p_list);
	~Database();
	void insert(Player p_player, int p_position);
	void remove(int p_position);
	void removeItr(DListIterator<Player> p_iter);
	std::vector<Player> searchSingle(int p_level);
	void searchRange(std::string p_toSearch);
	void searchFirstName(std::string p_toSearch);
	void removeDuplicates();
	bool removeDuplicatescheck(std::string firstName, std::string lastName);
	void averageXp(int p_level);
	void printAll();


};

#endif