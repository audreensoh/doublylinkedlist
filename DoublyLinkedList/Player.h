#ifndef PLAYER_H
#define PLAYER_H

/**
Contains implementations of player details
**/

#include<iostream>

class Player
{
private:
	std::string m_firstName;
	std::string m_lastName;
	int m_level;
	int m_xp;
public:
	Player();
	Player(std::string firstName, std::string lastName, int level, int xp);
	~Player();
	void setFirstName(std::string firstName);
	void setLastName(std::string lastName);
	void setLevel(int level);
	void setXp(int xp);
	std::string getFirstName();
	std::string getLastName();
	int getlevel();
	int getXp();
	void print();


};

#endif